﻿
Console.WriteLine("How many numbers do you want to sum up?");


string amountText = Console.ReadLine();
int amount = Convert.ToInt32(amountText);

int[] numbers = new int[amount];
Action[] numberFunctions = new Action[amount];

for (int i = 0; i < numbers.Length; i++)
{
	Console.WriteLine($"Enter number {i}: ");
	string numberText = Console.ReadLine();
	int number = Convert.ToInt32(numberText);
	numbers[i] = number;

	int iCopy = i;
	numberFunctions[i] = () => Console.WriteLine($"i is {iCopy}");
	Console.WriteLine($"i is {i}");
}


int sum = numbers.Sum();

Console.WriteLine($"Sum is: {sum}");


foreach (Action function in numberFunctions)
{
	function.Invoke();
}
